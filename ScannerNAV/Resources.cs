﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Søg ressource")]
    public class Resources : Activity
    {
        private EditText etSearchResourceNo;
        private EditText etSearchResourceName;
        private ListView listView;
        private List<ResourcesearchModel> ResourceList;
        private resourcesearch[] items;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.ResourceList);
            
            etSearchResourceNo = FindViewById<EditText>(Resource.Id.etSearchResourceNo);
            etSearchResourceNo.RequestFocus();

            etSearchResourceName = FindViewById<EditText>(Resource.Id.etSearchResourceName);
            etSearchResourceName.KeyPress += OnSearchItemName_KeyPress; ;

            listView = FindViewById<ListView>(Resource.Id.listView);
            listView.ItemClick += ListView_ItemClick;
        }

        private void OnSearchItemName_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;

                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    resourcesearch_root ResponseRoot = new resourcesearch_root();

                    ws.GetResourcesSearch(ref ResponseRoot, etSearchResourceNo.Text, etSearchResourceName.Text);

                    try
                    {
                        resourcesearch_response XmlResponse = ResponseRoot.resourcesearch_response[0];

                        if (Helper.IsOK(XmlResponse.status))
                        {
                            string ResourceNo = Helper.GetRescourceNo();
                            ResourceList = new List<ResourcesearchModel>(XmlResponse.resourcesearch.Length);
                            items = XmlResponse.resourcesearch;
                            foreach (resourcesearch item in XmlResponse.resourcesearch)
                            {
                                ResourceList.Add(new ResourcesearchModel { ResourceNo = item.no, ResourceName = item.name });
                            }
                            listView.Adapter = new ResourcesearchModelAdapter(this, ResourceList);
                            etSearchResourceNo.RequestFocus();

                            if ((XmlResponse.resourcesearch.Length == 1) && (!ResourceList[0].ResourceNo.Equals("")))
                            {
                                ResourcesearchModel selectedItem = new ResourcesearchModel();
                                selectedItem = ResourceList[0];
                                Intent intent = new Intent(this, typeof(OrderCard));
                                intent.PutExtra("ResourceNo", selectedItem.ResourceNo);
                                intent.PutExtra("ResourceName", selectedItem.ResourceName);
                                SetResult(Result.Ok, intent);
                                Finish();
                            }
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                    catch (Exception)
                    {
                        Helper.ShowAlertDialog(this, "Information", "Ingen ressource blev fundet");
                    }

                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
            else
                e.Handled = false;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ResourcesearchModel selectedItem = new ResourcesearchModel();
            selectedItem = ResourceList[e.Position];
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            createPurchaseLine.PutExtra("ResourceNo", selectedItem.ResourceNo);
            createPurchaseLine.PutExtra("ResourceName", selectedItem.ResourceName);
            SetResult(Result.Ok, createPurchaseLine);
            Finish();
        }

        override public void OnBackPressed()
        {
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            SetResult(Result.Canceled, createPurchaseLine);
            Finish();
        }
    }
}