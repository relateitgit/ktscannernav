﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Søg vare")]
    public class ItemLocations : Activity
    {
        private string itemNo;
        private ListView listView;
        List<ItemLocationModel> itemList;
        private item_location[] items;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.ItemLocation);

            itemNo = Intent.GetStringExtra("ItemNo") ?? "Data not available";

            listView = FindViewById<ListView>(Resource.Id.listView);
            listView.ItemClick += ListView_ItemClick;

            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                item_location_root ResponseRoot = new item_location_root();

                ws.GetItemLocation(ref ResponseRoot, itemNo);

                item_location_response XmlResponse = ResponseRoot.item_location_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    itemList = new List<ItemLocationModel>(XmlResponse.item_location.Length);
                    items = XmlResponse.item_location;
                    foreach (item_location item in XmlResponse.item_location)
                    {
                        itemList.Add(new ItemLocationModel
                        {
                            Code = item.Loc_code,
                            Name = item.Loc_name,
                            Qty = item.Loc_qty

                        });
                    }
                    listView.Adapter = new ItemLocationModelAdapter(this, itemList);
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }

            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ItemLocationModel selectedItem = new ItemLocationModel();
            selectedItem = itemList[e.Position];
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            createPurchaseLine.PutExtra("LocationNo", selectedItem.Code);
            SetResult(Result.Ok, createPurchaseLine);
            Finish();
        }

        override public void OnBackPressed()
        {
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            SetResult(Result.Canceled, createPurchaseLine);
            Finish();
        }
    }
}