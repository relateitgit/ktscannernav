﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Opret købslinje", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PurchaseLineCard : Activity
    {
        private EditText etItemNo;
        private EditText etQty;
        private TextView tvStatus;
        private string purchaseNo;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.PurchaseLineCard);

            purchaseNo = Intent.GetStringExtra("PONo") ?? "Data not available";

            etItemNo = FindViewById<EditText>(Resource.Id.etItemNo);
            etItemNo.RequestFocus();

            etQty = FindViewById<EditText>(Resource.Id.etQty);
           
            FindViewById<Button>(Resource.Id.btnSearch).Click += OnSearch_Click;

            FindViewById<Button>(Resource.Id.btnCreateReceive).Click += OnCreateReceive_Click;

            tvStatus = FindViewById<TextView>(Resource.Id.tvStatus);
            tvStatus.Click += OnStatusClick;
            tvStatus.Text = Helper.GetStatus();
        }
        
        private void OnSearch_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Items));
            StartActivityForResult(intent, 1);
        }

        private void OnCreateReceive_Click(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                Int32.TryParse(etQty.Text, out int qty);
                ws.CreatePurchaseLine(ref navResponse, purchaseNo, etItemNo.Text, qty);

                default_response xmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(xmlResponse.status))
                {
                    Helper.ShowToast(this, xmlResponse.status_text);
                    Finish();
                }
                else
                {
                    Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }        

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == 0)
            {
                base.OnActivityResult(requestCode, resultCode, data);
                tvStatus.Text = Helper.GetStatus();
            }
            else if (requestCode == 1)
            {                
                var ItemNo = data.GetStringExtra("ItemNo") ?? "Data not available";
                etItemNo.Text = ItemNo;
            }            
        }
    }
}