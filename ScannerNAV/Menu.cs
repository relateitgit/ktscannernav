﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Menu : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Menu);

            FindViewById<Button>(Resource.Id.btnMoveLocation).Click += OnMoveLocation_Click;

            FindViewById<Button>(Resource.Id.btnPrintLabel).Click += OnPrintLabel_Click;

            FindViewById<Button>(Resource.Id.btnReceiveItem).Click += OnItemReceive_Click;

            FindViewById<Button>(Resource.Id.btnInfo).Click += OnInfo_Click;

            FindViewById<Button>(Resource.Id.btnComment).Click += OnComment_Click;

            FindViewById<Button>(Resource.Id.btnItemInfo).Click += OnItemInfo_Click;

            FindViewById<Button>(Resource.Id.btnOrders).Click += OnOrders_Click; ;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = "Bruger: " + Helper.GetStatus();
        }

        private void OnOrders_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(OrderSearch));
        }

        private void OnItemInfo_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ItemInfoSearch));
        }

        private void OnInfo_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(TrailerInfo));
        }

        private void OnComment_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(TrailerComment));
        }

        private void OnItemReceive_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ReceiveItem_POrders));
        }

        private void OnPrintLabel_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(PrintLabel));
        }

        private void OnMoveLocation_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(MoveLocationBatch));
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("Log af");
            dialog.SetMessage("Vil du gerne logge af?");
            dialog.SetPositiveButton("OK", OkAction);
            dialog.SetNegativeButton("Annuller", CancelAction);
            var myCustomDialog = dialog.Create();
            myCustomDialog.Show();
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            base.OnBackPressed();
        }

        private void CancelAction(object sender, DialogClickEventArgs e)
        {
            //Do nothing
        }
    }
}