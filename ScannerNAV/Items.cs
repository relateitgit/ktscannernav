﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Søg vare")]
    public class Items : Activity
    {
        private EditText etSearchItemNo;
        private EditText etSearchItemDesc;
        private ListView listView;
        List<ItemsModel> itemList;
        private item[] items;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.ItemList);
            
            etSearchItemNo = FindViewById<EditText>(Resource.Id.etSearchItemNo);
            etSearchItemNo.RequestFocus();

            etSearchItemDesc = FindViewById<EditText>(Resource.Id.etSearchItemDesc);
            etSearchItemDesc.KeyPress += OnSearchItemDesc_KeyPress; ;

            listView = FindViewById<ListView>(Resource.Id.listView);
            listView.ItemClick += ListView_ItemClick;
        }

        private void OnSearchItemDesc_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;

                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    item_root ResponseRoot = new item_root();

                    ws.GetItems(ref ResponseRoot, etSearchItemNo.Text, etSearchItemDesc.Text);

                    try
                    {
                        item_response XmlResponse = ResponseRoot.item_response[0];

                        if (Helper.IsOK(XmlResponse.status))
                        {
                            string ResourceNo = Helper.GetRescourceNo();
                            itemList = new List<ItemsModel>(XmlResponse.item.Length);
                            items = XmlResponse.item;
                            foreach (item item in XmlResponse.item)
                            {
                                itemList.Add(new ItemsModel { ItemNo = item.no, ItemDesc = item.description });
                            }
                            listView.Adapter = new ItemsModelAdapter(this, itemList);
                            etSearchItemNo.RequestFocus();

                            if ((XmlResponse.item.Length == 1) && (!itemList[0].ItemNo.Equals("")))
                            {
                                ItemsModel selectedItem = new ItemsModel();
                                selectedItem = itemList[0];
                                Intent intent = new Intent(this, typeof(OrderCard));
                                intent.PutExtra("ItemNo", selectedItem.ItemNo);
                                intent.PutExtra("ItemDesc", selectedItem.ItemDesc);
                                SetResult(Result.Ok, intent);
                                Finish();
                            }
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                    catch (Exception)
                    {
                        Helper.ShowAlertDialog(this, "Information", "Ingen vare blev fundet");
                    }

                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
            else
                e.Handled = false;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ItemsModel selectedItem = new ItemsModel();
            selectedItem = itemList[e.Position];
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            createPurchaseLine.PutExtra("ItemNo", selectedItem.ItemNo);
            createPurchaseLine.PutExtra("ItemDesc", selectedItem.ItemDesc);
            SetResult(Result.Ok, createPurchaseLine);
            Finish();
        }

        override public void OnBackPressed()
        {
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            SetResult(Result.Canceled, createPurchaseLine);
            Finish();
        }
    }
}