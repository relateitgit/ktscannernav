﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;

namespace ScannerNAV
{
    [Activity(Label = "Flyt lokation", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MoveLocation : Activity
    {
        private Button btnMove;
        private EditText etSearchText;
        private TextView tvBin;
        private Spinner spnBin;
        private EditText etBin;
        private trailer_bin[] trailerBinList;
        private TextView tvStatus;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.MoveLocation);

            etSearchText = FindViewById<EditText>(Resource.Id.etSearchText);
            etSearchText.KeyPress += OnSearchText_KeyPress;
            etSearchText.RequestFocus();

            tvBin = FindViewById<TextView>(Resource.Id.tvBin);
            tvBin.Visibility = ViewStates.Invisible;

            spnBin = FindViewById<Spinner>(Resource.Id.spnBin);
            spnBin.Visibility = ViewStates.Invisible;

            etBin = FindViewById<EditText>(Resource.Id.etBin);
            etBin.KeyPress += OnBin_KeyPress;
            etBin.Visibility = ViewStates.Invisible;

            btnMove = FindViewById<Button>(Resource.Id.btnMove);
            btnMove.Click += OnMoveClick;

            tvStatus = FindViewById<TextView>(Resource.Id.tvStatus);
            tvStatus.Click += OnStatusClick;
            tvStatus.Text = Helper.GetStatus();
        }

        private void OnBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                btnMove.CallOnClick();                
            }
            else
                e.Handled = false;
        }

        private void OnSearchText_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    trailer_bin_root navResponse = new trailer_bin_root();

                    ws.FindTrailerBinMove(ref navResponse, Helper.GetRescourceNo(), etSearchText.Text);

                    trailer_bin_response xmlResponse = navResponse.trailer_bin_response[0];

                    if (Helper.IsOK(xmlResponse.status))
                    {
                        int counter = 0;
                        int selectedItem = 0;
                        string CurrBin = xmlResponse.status_text;
                        List<string> items = new List<string>(xmlResponse.trailer_bin.Length);
                        trailerBinList = xmlResponse.trailer_bin;
                        foreach (trailer_bin item in xmlResponse.trailer_bin)
                        {
                            items.Add(item.no + " - " + item.name);
                            if (item.no == CurrBin)
                            {
                                selectedItem = counter;
                            }
                            counter++;
                        }
                        ArrayAdapter adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                        spnBin.Adapter = adapter;
                        spnBin.SetSelection(selectedItem);

                        tvBin.Visibility = ViewStates.Visible;
                        //spnBin.Visibility = ViewStates.Visible;
                        etBin.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
                e.Handled = true;                
            }
            else
                e.Handled = false;
        }

        private void OnMoveClick(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                //ws.MoveLocation(ref navResponse, Helper.GetRescourceNo(), etSearchText.Text, trailerBinList[spnBin.SelectedItemPosition].no);
                ws.MoveLocation(ref navResponse, Helper.GetRescourceNo(), etSearchText.Text, etBin.Text);

                default_response xmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(xmlResponse.status))
                {
                    Helper.ShowToast(this, xmlResponse.status_text);
                    etSearchText.Text = "";
                    etBin.Text = "";
                    tvBin.Visibility = ViewStates.Invisible;
                    spnBin.Visibility = ViewStates.Invisible;
                    etBin.Visibility = ViewStates.Invisible;
                }
                else
                {
                    Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            tvStatus.Text = Helper.GetStatus();
        }
    }
}