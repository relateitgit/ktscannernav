﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Order Card", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class OrderLineCard : Activity
    {
        private string OrderNo;
        private int LineNo;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.OrderLineCard);

            OrderNo = Intent.GetStringExtra("OrderNo") ?? "Data not available";
            LineNo = Intent.GetIntExtra("LineNo", 0);

            FindViewById<Button>(Resource.Id.btnItemSearch).Click += OnItemSearch_Click;
            FindViewById<Button>(Resource.Id.btnLocationSearch).Click += OnLocationSearch_Click;
            FindViewById<Button>(Resource.Id.btnSave).Click += OnSave_Click;
            FindViewById<Button>(Resource.Id.btnDelete).Click += OnDelete_Click;

            if (LineNo == 0)
            {
                FindViewById<TextView>(Resource.Id.etQuantity).Text = "1";
                FindViewById<RadioButton>(Resource.Id.radio_item).Checked = true;
            }
            else
            {
                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    sale_lines_root ResponseLineRoot = new sale_lines_root();

                    ws.GetSaleLines(ref ResponseLineRoot, OrderNo, LineNo);

                    sale_lines_response XmlResponseLine = ResponseLineRoot.sale_lines_response[0];

                    if (Helper.IsOK(XmlResponseLine.status))
                    {
                        sale_lines sale_Line = XmlResponseLine.sale_lines[0];
                        FindViewById<TextView>(Resource.Id.etItemNo).Text = sale_Line.item_no;
                        FindViewById<TextView>(Resource.Id.tvDescription2).Text = sale_Line.description;
                        FindViewById<TextView>(Resource.Id.etQuantity).Text = sale_Line.quantity;
                        FindViewById<TextView>(Resource.Id.etLocation).Text = sale_Line.location_code;
                        if (sale_Line.line_type == "2")
                        {
                            FindViewById<RadioButton>(Resource.Id.radio_item).Checked = true;
                        }
                        else if (sale_Line.line_type == "3")
                        {
                            FindViewById<RadioButton>(Resource.Id.radio_res).Checked = true;
                        }
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponseLine.status, XmlResponseLine.status_text);
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
        }

        private void OnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root ResponseRoot = new default_root();

                ws.DeleteSalesLine(ref ResponseRoot, OrderNo, LineNo);

                default_response XmlResponse = ResponseRoot.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    Finish();
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnItemSearch_Click(object sender, EventArgs e)
        {
            if (FindViewById<RadioButton>(Resource.Id.radio_item).Checked)
            {
                Intent intent = new Intent(this, typeof(Items));
                StartActivityForResult(intent, 0);
            }
            else if (FindViewById<RadioButton>(Resource.Id.radio_res).Checked)
            {
                Intent intent = new Intent(this, typeof(Resources));
                StartActivityForResult(intent, 2);
            }
        }

        private void OnLocationSearch_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ItemLocations));
            intent.PutExtra("ItemNo", FindViewById<TextView>(Resource.Id.etItemNo).Text);
            StartActivityForResult(intent, 1);
        }

        private void OnSave_Click(object sender, EventArgs e)
        {
            if (FindViewById<RadioButton>(Resource.Id.radio_item).Checked)
            {
                if (string.IsNullOrWhiteSpace(FindViewById<TextView>(Resource.Id.etLocation).Text))
                {
                    Helper.ShowAlertDialog(this, "ERROR", "Lokationen skal være udfyldt");
                }
                else
                {
                    try
                    {
                        ScannerInterface ws = Helper.GetInterface(this);

                        default_root ResponseRoot = new default_root();

                        ws.SetNewSalesLine(ref ResponseRoot, Helper.GetRescourceNo(), OrderNo, LineNo, FindViewById<TextView>(Resource.Id.etItemNo).Text, FindViewById<TextView>(Resource.Id.etQuantity).Text, FindViewById<TextView>(Resource.Id.etLocation).Text);

                        default_response XmlResponse = ResponseRoot.default_response[0];

                        if (Helper.IsOK(XmlResponse.status))
                        {
                            Finish();
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                    }
                }
            }
            else if (FindViewById<RadioButton>(Resource.Id.radio_res).Checked)
            {
                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root ResponseRoot = new default_root();

                    ws.SetNewSalesLineResource(ref ResponseRoot, Helper.GetRescourceNo(), OrderNo, LineNo, FindViewById<TextView>(Resource.Id.etItemNo).Text, FindViewById<TextView>(Resource.Id.etQuantity).Text, FindViewById<TextView>(Resource.Id.etLocation).Text);

                    default_response XmlResponse = ResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Finish();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }            
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (resultCode == Result.Ok)
            {
                if (requestCode == 0)
                {
                    FindViewById<TextView>(Resource.Id.etItemNo).Text = data.GetStringExtra("ItemNo") ?? "Data not available";
                    FindViewById<TextView>(Resource.Id.tvDescription2).Text = data.GetStringExtra("ItemDesc") ?? "Data not available";
                    FindViewById<TextView>(Resource.Id.etLocation).Text = "";
                }
                else if (requestCode == 1)
                {
                    FindViewById<TextView>(Resource.Id.etLocation).Text = data.GetStringExtra("LocationNo") ?? "Data not available";
                }
                else if (requestCode == 2)
                {
                    FindViewById<TextView>(Resource.Id.etItemNo).Text = data.GetStringExtra("ResourceNo") ?? "Data not available";
                    FindViewById<TextView>(Resource.Id.tvDescription2).Text = data.GetStringExtra("ResourceName") ?? "Data not available";
                    FindViewById<TextView>(Resource.Id.etLocation).Text = "";
                }
            }
        }
    }
}