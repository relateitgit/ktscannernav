﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using ScannerNAV.Webservice;
using System.Collections.Generic;

namespace ScannerNAV
{
    [Activity(Label = "Trailer Kommentar kort", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class TrailerCommentCard : Activity
    {
        private EditText etNewComment;
        private Button btnSave;
        private ListView listView;
        List<CommentModel> items;

        private string serial;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.TrailerCommentCard);
            
            serial = Intent.GetStringExtra("Serial") ?? "Data not available";

            listView = FindViewById<ListView>(Resource.Id.listView);

            etNewComment = FindViewById<EditText>(Resource.Id.etNewComment);

            btnSave = FindViewById<Button>(Resource.Id.btnSave);
            btnSave.Click += OnSave_Click;

            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                comment_root ResponseRoot = new comment_root();

                ws.GetComments(ref ResponseRoot, serial);

                try
                {
                    comment_response XmlResponse = ResponseRoot.comment_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        items = new List<CommentModel>(XmlResponse.comment.Length);                     
                        foreach (comment item in XmlResponse.comment)
                        {
                            items.Add(new CommentModel { Date = item.date, Comment = item.comment_text });
                        }
                        listView.Adapter = new CommentModelAdapter(this, items);
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
                catch (Exception)
                {                    
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root default_root = new default_root();

                ws.SetComment(ref default_root, serial, etNewComment.Text);

                default_response XmlResponse = default_root.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    etNewComment.Text = "";
                    Recreate();
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }
    }
}