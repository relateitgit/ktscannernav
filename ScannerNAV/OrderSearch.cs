﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Søg ordre")]
    public class OrderSearch : Activity
    {
        List<SalesOrderModel> itemList;
        private sale_orders[] items;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.OrderSearch);

            FindViewById<EditText>(Resource.Id.etSearchOrder).RequestFocus();
            FindViewById<EditText>(Resource.Id.etSearchOrder).KeyPress += OnSearch_KeyPress;

            FindViewById<ListView>(Resource.Id.listView).ItemClick += ListView_ItemClick;
        }

        private void OnSearch_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;

                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    sale_orders_root ResponseRoot = new sale_orders_root();

                    ws.GetSaleOrders(ref ResponseRoot, FindViewById<EditText>(Resource.Id.etSearchOrder).Text);

                    try
                    {
                        sale_orders_response XmlResponse = ResponseRoot.sale_orders_response[0];

                        if (Helper.IsOK(XmlResponse.status))
                        {
                            itemList = new List<SalesOrderModel>(XmlResponse.sale_orders.Length);
                            items = XmlResponse.sale_orders;
                            foreach (sale_orders item in XmlResponse.sale_orders)
                            {
                                itemList.Add(new SalesOrderModel
                                {
                                    OrderNo = item.no,
                                    CustomerNo = item.sell_to_no,
                                    CustomerName = item.sell_to_name
                                });
                            }
                            FindViewById<ListView>(Resource.Id.listView).Adapter = new SalesOrderModelAdapter(this, itemList);
                            FindViewById<EditText>(Resource.Id.etSearchOrder).RequestFocus();
                            
                            if ((XmlResponse.sale_orders.Length == 1) && (!itemList[0].OrderNo.Equals("")))
                            {
                                SalesOrderModel selectedItem = new SalesOrderModel();
                                selectedItem = itemList[0];
                                Intent card = new Intent(this, typeof(OrderCard));
                                card.PutExtra("OrderNo", selectedItem.OrderNo);
                                StartActivity(card);
                            }
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                    catch (Exception)
                    {
                        Helper.ShowAlertDialog(this, "Information", "Ingen ordre blev fundet");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
            else
                e.Handled = false;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            SalesOrderModel selectedItem = new SalesOrderModel();
            selectedItem = itemList[e.Position];
            Intent card = new Intent(this, typeof(OrderCard));
            card.PutExtra("OrderNo", selectedItem.OrderNo);
            StartActivity(card);
        }
    }
}