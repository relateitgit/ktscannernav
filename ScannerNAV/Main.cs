﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using Android.Views;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Main : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            RequestWindowFeature(WindowFeatures.NoTitle);

            if (Helper.GetWSUser() == null)
            {
                Helper.SetWSUser("Kongeaa\\sclog");
                Helper.SetWSPasword("konGe8");//"E$m`Yu}32)4G~k&}");
                Helper.SetWSUrl("https://nav.kongeaa.dk:7049/Kongeaa/WS/Kongeaa%20Trailercenter%20A%2FS/Codeunit/ScannerInterface");                
            }

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            FindViewById<Button>(Resource.Id.btnLogin).Click += OnLoginClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnConnectionsSettingsClick;
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);
        }

        private void OnLoginClick(object sender, System.EventArgs e)
        {
            try
            {
                Intent intent = new Intent(this, typeof(LogIn));
                StartActivityForResult(intent, 1);
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);                
            }
        }
        
        private void OnConnectionsSettingsClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(SettingsConnect));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if((requestCode == 1) & (resultCode == Result.Ok))
            {
                StartActivity(typeof(Menu));
            }
        }
    }
}

