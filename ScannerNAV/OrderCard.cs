﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Order Card", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class OrderCard : Activity
    {
        private string OrderNo;
        List<SalesOrderLineModel> salesLineList;
        private sale_lines[] sale_Lines;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.OrderCard);

            OrderNo = Intent.GetStringExtra("OrderNo") ?? "Data not available";

            FindViewById<ListView>(Resource.Id.listView).ItemClick += ListView_ItemClick;
            FindViewById<Button>(Resource.Id.btnNewLine).Click += NewLine_Click;

            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                sale_orders_root ResponseRoot = new sale_orders_root();

                ws.GetSaleOrders(ref ResponseRoot, OrderNo);

                sale_orders_response XmlResponse = ResponseRoot.sale_orders_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    sale_orders sale_order = XmlResponse.sale_orders[0];
                    FindViewById<TextView>(Resource.Id.tvOrderNo2).Text = sale_order.no;
                    FindViewById<TextView>(Resource.Id.tvCustomerNo2).Text = sale_order.sell_to_no;
                    FindViewById<TextView>(Resource.Id.tvCustomerName2).Text = sale_order.sell_to_name;
                    
                    sale_lines_root ResponseLineRoot = new sale_lines_root();

                    ws.GetSaleLines(ref ResponseLineRoot, OrderNo, 0);

                    sale_lines_response XmlResponseLine = ResponseLineRoot.sale_lines_response[0];

                    if (Helper.IsOK(XmlResponseLine.status))
                    {
                        salesLineList = new List<SalesOrderLineModel>(XmlResponseLine.sale_lines.Length);
                        sale_Lines = XmlResponseLine.sale_lines;
                        foreach (sale_lines saleLine in XmlResponseLine.sale_lines)
                        {
                            salesLineList.Add(new SalesOrderLineModel
                            {
                                LineNo = saleLine.line_no,
                                ItemNo = saleLine.item_no,
                                Description = saleLine.description,
                                quantity = saleLine.quantity,
                                location_code = saleLine.location_code,
                                LineType = saleLine.line_type
                            });
                        }
                        FindViewById<ListView>(Resource.Id.listView).Adapter = new SalesOrderLineModelAdapter(this, salesLineList);
                    }
                    else
                    {
                        //Helper.ShowAlertDialog(this, XmlResponseLine.status, XmlResponseLine.status_text);
                    }
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void NewLine_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(OrderLineCard));
            intent.PutExtra("OrderNo", OrderNo);
            intent.PutExtra("LineNo", 0);
            StartActivityForResult(intent, 0);
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            SalesOrderLineModel selectedItem = new SalesOrderLineModel();
            selectedItem = salesLineList[e.Position];
            Intent intent = new Intent(this, typeof(OrderLineCard));
            intent.PutExtra("OrderNo", OrderNo);
            intent.PutExtra("LineNo", selectedItem.LineNo);
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            Recreate();
        }
    }
}