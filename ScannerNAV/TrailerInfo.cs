﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Trailer Information")]
    public class TrailerInfo : Activity
    {
        private EditText etSearchTrailerID;
        private EditText etSearchSerial;
        private ListView listView;
        List<TrailerInfoModel> items;
        private trailer[] Trailers;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.TrailerSearch);

            etSearchTrailerID = FindViewById<EditText>(Resource.Id.etSearchTrailerID);
            etSearchTrailerID.RequestFocus();

            etSearchSerial = FindViewById<EditText>(Resource.Id.etSearchSerial);
            etSearchSerial.KeyPress += OnSearchSerial_KeyPress;            

            listView = FindViewById<ListView>(Resource.Id.listView);
            listView.ItemClick += ListView_ItemClick;
        }

        private void OnSearchSerial_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;

                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    trailer_root ResponseRoot = new trailer_root();

                    ws.FindTrailer(ref ResponseRoot,Helper.GetRescourceNo(), etSearchTrailerID.Text, etSearchSerial.Text);

                    try
                    {
                        trailer_response XmlResponse = ResponseRoot.trailer_response[0];

                        if (Helper.IsOK(XmlResponse.status))
                        {
                            items = new List<TrailerInfoModel>(XmlResponse.trailer.Length);
                            Trailers = XmlResponse.trailer;
                            foreach (trailer item in XmlResponse.trailer)
                            {
                                items.Add(new TrailerInfoModel { TrailerID = item.trailer_id, Serial = item.serial_no });
                            }
                            listView.Adapter = new TrailerInfoModelAdapter(this, items);
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                    catch (Exception)
                    {
                        Helper.ShowAlertDialog(this, "Information", "Ingen trailer blev fundet");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
            else
                e.Handled = false;
        }
        
        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            TrailerInfoModel selectedItem = new TrailerInfoModel();
            selectedItem = items[e.Position];
            Intent trailerInfoCard = new Intent(this, typeof(TrailerInfoCard));
            trailerInfoCard.PutExtra("Serial", selectedItem.Serial);
            StartActivity(trailerInfoCard);
        }
    }
}