﻿using System;
using ScannerNAV.Webservice;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "PrintLabel")]
    public class PrintLabel : Activity
    {
        private Button btnPrint;
        private EditText etSearchText;
        private TextView tvStatus;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);
            
            SetContentView(Resource.Layout.PrintLabel);

            etSearchText = FindViewById<EditText>(Resource.Id.etSearchText);
            etSearchText.KeyPress += OnSearchText_KeyPress;
            etSearchText.RequestFocus();

            btnPrint = FindViewById<Button>(Resource.Id.btnPrint);
            btnPrint.Click += OnPrintClick;

            tvStatus = FindViewById<TextView>(Resource.Id.tvStatus);
            tvStatus.Click += OnStatusClick;
            tvStatus.Text = Helper.GetStatus();
        }

        private void OnSearchText_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                btnPrint.CallOnClick();                                
            }
            else
                e.Handled = false;
        }

        private void OnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                ws.PrintLabel(ref navResponse, Helper.GetRescourceNo(), etSearchText.Text);

                default_response xmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(xmlResponse.status))
                {
                    Helper.ShowToast(this, xmlResponse.status_text);
                    etSearchText.Text = "";
                }
                else
                {
                    Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            tvStatus.Text = Helper.GetStatus();
        }
    }
}