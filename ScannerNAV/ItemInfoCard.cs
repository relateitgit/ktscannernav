﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Item Information Card", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ItemInfoCard : Activity
    {
        private string ItemNo;
        List<ItemLocationModel> locationList;
        private item_location[] item_locations;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.ItemInfoCard);

            ItemNo = Intent.GetStringExtra("ItemNo") ?? "Data not available";

            FindViewById<ListView>(Resource.Id.listView).ItemClick += ListView_ItemClick;

            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                item_root item_root = new item_root();

                ws.GetItems(ref item_root, ItemNo, "");

                item_response XmlResponse = item_root.item_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    item item = XmlResponse.item[0];
                    FindViewById<TextView>(Resource.Id.tvItemNo2).Text = item.no;
                    FindViewById<TextView>(Resource.Id.tvDesc_2).Text = item.description;
                    FindViewById<TextView>(Resource.Id.tvDesc2_2).Text = item.description2;
                    FindViewById<TextView>(Resource.Id.tvAvalibleStock2).Text = item.avalible_stock;
                    FindViewById<TextView>(Resource.Id.tvQtyPO2).Text = item.qty_purch_order;
                    FindViewById<TextView>(Resource.Id.tvUnitPrice2).Text = item.unit_price;
                    FindViewById<TextView>(Resource.Id.tvUnitPriceVAT2).Text = item.unit_price_vat;
                    FindViewById<TextView>(Resource.Id.tvCostCode2).Text = item.cost_code;

                    item_location_root item_loc_root = new item_location_root();

                    ws.GetItemLocation(ref item_loc_root, ItemNo);

                    item_location_response XmlResponseLoc = item_loc_root.item_location_response[0];

                    if (Helper.IsOK(XmlResponseLoc.status))
                    {
                        locationList = new List<ItemLocationModel>(XmlResponseLoc.item_location.Length);
                        item_locations = XmlResponseLoc.item_location;
                        foreach (item_location item_location in XmlResponseLoc.item_location)
                        {
                            locationList.Add(new ItemLocationModel
                            {
                                Code = item_location.Loc_code,
                                Name = item_location.Loc_name,
                                Qty = item_location.Loc_qty
                            });
                        }
                        FindViewById<ListView>(Resource.Id.listView).Adapter = new ItemLocationModelAdapter(this, locationList);
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ItemLocationModel selectedItem = new ItemLocationModel();
            selectedItem = locationList[e.Position];
            Intent intent = new Intent(this, typeof(ItemMoveCard));
            intent.PutExtra("LocationCode", selectedItem.Code);
            intent.PutExtra("ItemNo", ItemNo);
            intent.PutExtra("ItemDescription", FindViewById<TextView>(Resource.Id.tvDesc_2).Text);
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            Recreate();
        }
    }
}