﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;

namespace ScannerNAV
{
    [Activity(Label = "Flyt lokation", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MoveLocationBatch : Activity
    {
        private Button btnMove;
        private EditText etSearchText;
        private TextView tvBin;
        private EditText etBin;
        private ListView listView;
        private TextView tvStatus;
        private int Counter = 0;
        List<string> items;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.MoveLocationBatch);

            etSearchText = FindViewById<EditText>(Resource.Id.etSearchText);
            etSearchText.KeyPress += OnSearchText_KeyPress;
            etSearchText.RequestFocus();

            tvBin = FindViewById<TextView>(Resource.Id.tvBin);
            tvBin.Visibility = ViewStates.Invisible;

            etBin = FindViewById<EditText>(Resource.Id.etBin);
            etBin.KeyPress += OnBin_KeyPress;
            etBin.Visibility = ViewStates.Invisible;

            btnMove = FindViewById<Button>(Resource.Id.btnMove);
            btnMove.Click += OnMoveClick;

            listView = FindViewById<ListView>(Resource.Id.listView);

            tvStatus = FindViewById<TextView>(Resource.Id.tvStatus);
            tvStatus.Click += OnStatusClick;
            tvStatus.Text = Helper.GetStatus();
        }

        private void OnBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                btnMove.CallOnClick();
            }
            else
                e.Handled = false;
        }

        private void OnSearchText_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    trailer_bin_root navResponse = new trailer_bin_root();

                    ws.FindTrailerBinMove(ref navResponse, Helper.GetRescourceNo(), etSearchText.Text);

                    trailer_bin_response xmlResponse = navResponse.trailer_bin_response[0];

                    if (Helper.IsOK(xmlResponse.status))
                    {
                        if (items == null)
                        {
                            items = new List<string>();
                        }
                        Counter = items.Count + 1;
                        items.Add(etSearchText.Text);
                        listView.Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleGalleryItem, items);

                        tvBin.Visibility = ViewStates.Visible;
                        etBin.Visibility = ViewStates.Visible;
                        etSearchText.Text = "";
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
                e.Handled = true;
            }
            else
                e.Handled = false;
        }

        private void OnMoveClick(object sender, EventArgs e)
        {
            string Itemlist = "";

            foreach (string item in items)
            {
                if (Itemlist == "")
                {
                    Itemlist += item;
                }
                else
                {
                    Itemlist += "," + item;
                }
            }

            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                ws.MoveLocationBatch(ref navResponse, Helper.GetRescourceNo(), Itemlist, etBin.Text);

                default_response xmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(xmlResponse.status))
                {
                    Helper.ShowToast(this, xmlResponse.status_text);
                    tvBin.Visibility = ViewStates.Invisible;
                    etBin.Visibility = ViewStates.Invisible;

                    etBin.Text = "";
                    items.Clear();
                    Recreate();
                }
                else
                {
                    Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            tvStatus.Text = Helper.GetStatus();
        }
    }
}