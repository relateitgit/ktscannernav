﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using ScannerNAV.Webservice;
using System.Collections.Generic;
using Android.Views;

namespace ScannerNAV
{
    [Activity(Label = "Settings", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Settings : Activity
    {
        private ScannerInterface ws;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                base.OnCreate(savedInstanceState);
                RequestWindowFeature(WindowFeatures.NoTitle);

                // Create your application here
                SetContentView(Resource.Layout.Settings);

                FindViewById<Button>(Resource.Id.btnSave).Click += OnSave;

                RefreshPage();
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void RefreshPage()
        {
            ws = Helper.GetInterface(this);
            resource_root resourceRoot = new resource_root();
 
            ws.GetResourcesInfo(ref resourceRoot, Helper.GetRescourceNo());

            resource_response resourceInfoXmlResponse = resourceRoot.resource_response[0];

            if (Helper.IsOK(resourceInfoXmlResponse.status))
            {
                resource resourceInfo = resourceInfoXmlResponse.resource[0];
                FindViewById<TextView>(Resource.Id.tvResourceNo).Text = resourceInfo.no;
                FindViewById<TextView>(Resource.Id.tvName).Text = resourceInfo.name;
                FindViewById<CheckBox>(Resource.Id.cbDiscountSL).Checked = resourceInfo.discount_sales_line != "0";
            }
            else
            {
                Helper.ShowAlertDialog(this, resourceInfoXmlResponse.status, resourceInfoXmlResponse.status_text);
            }
        }

        private void OnSave(object sender, EventArgs e)
        {            
            default_root defaultResponseRoot = new default_root();
            ws.SetResourcesInfo(ref defaultResponseRoot, Helper.GetRescourceNo(), FindViewById<CheckBox>(Resource.Id.cbDiscountSL).Checked);
            default_response xmlResponse = defaultResponseRoot.default_response[0];
            if (Helper.IsOK(xmlResponse.status))
            {
                Finish();
            }
            else
            {
                Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
            }            
        }
    }
}