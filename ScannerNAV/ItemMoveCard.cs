﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Item Move Card", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ItemMoveCard : Activity
    {
        private string LocationCode;
        private string ItemNo;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.ItemMoveCard);

            LocationCode = Intent.GetStringExtra("LocationCode") ?? "Data not available";
            ItemNo = Intent.GetStringExtra("ItemNo") ?? "Data not available";

            FindViewById<TextView>(Resource.Id.tvItemNo2).Text = ItemNo;
            FindViewById<TextView>(Resource.Id.tvDescription2).Text = Intent.GetStringExtra("ItemDescription") ?? "Data not available";
            FindViewById<TextView>(Resource.Id.tvLocation2).Text = LocationCode;
            FindViewById<Button>(Resource.Id.btnLocationSearch).Click += OnLocationSearch_Click;
            FindViewById<Button>(Resource.Id.btnSave).Click += OnSave_Click;
        }

        private void OnLocationSearch_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Locations));
            StartActivityForResult(intent, 1);
        }

        private void OnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FindViewById<TextView>(Resource.Id.etNewLocation).Text))
            {
                Helper.ShowAlertDialog(this, "ERROR", "Lokationen skal være udfyldt");
            }
            else if (string.IsNullOrWhiteSpace(FindViewById<TextView>(Resource.Id.etQuantity).Text))
            {
                Helper.ShowAlertDialog(this, "ERROR", "Antal skal være udfyldt");
            }
            else
            {
                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root ResponseRoot = new default_root();

                    ws.MoveItem(ref ResponseRoot, Helper.GetRescourceNo(), ItemNo, LocationCode, FindViewById<TextView>(Resource.Id.etNewLocation).Text, Convert.ToInt32(FindViewById<TextView>(Resource.Id.etQuantity).Text));

                    default_response XmlResponse = ResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Finish();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (resultCode == Result.Ok)
            {
                if (requestCode == 1)
                {
                    FindViewById<TextView>(Resource.Id.etNewLocation).Text = data.GetStringExtra("LocationNo") ?? "Data not available";
                }
            }
        }
    }
}