﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "Trailer Information Card", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class TrailerInfoCard : Activity
    {
        private TextView tvTrailerID2;
        private TextView tvSerialNo2;
        private TextView tvRegistration2;
        private TextView tvDesc2;
        private TextView tvBinCode2;
        private TextView tvItemNo2;
        private TextView tvItemDesc2;
        private TextView tvBlocked2;
        private string serial;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.TrailerInfoCard);
            
            serial = Intent.GetStringExtra("Serial") ?? "Data not available";

            tvTrailerID2 = FindViewById<TextView>(Resource.Id.tvTrailerID2);
            tvSerialNo2 = FindViewById<TextView>(Resource.Id.tvSerialNo2);
            tvRegistration2 = FindViewById<TextView>(Resource.Id.tvRegistration2);
            tvDesc2 = FindViewById<TextView>(Resource.Id.tvDesc2);
            tvBinCode2 = FindViewById<TextView>(Resource.Id.tvBinCode2);
            tvItemNo2 = FindViewById<TextView>(Resource.Id.tvItemNo2);
            tvItemDesc2 = FindViewById<TextView>(Resource.Id.tvItemDesc2);
            tvBlocked2 = FindViewById<TextView>(Resource.Id.tvBlocked2);

            try
            {                
                ScannerInterface ws = Helper.GetInterface(this);

                trailer_root trailer_root = new trailer_root();

                ws.FindTrailer(ref trailer_root, Helper.GetRescourceNo(), "", serial);

                trailer_response XmlResponse = trailer_root.trailer_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    trailer trailer = XmlResponse.trailer[0];
                    tvTrailerID2.Text = trailer.trailer_id;
                    tvSerialNo2.Text = trailer.serial_no;
                    tvRegistration2.Text = trailer.registration_no;
                    tvDesc2.Text = trailer.desc;
                    tvBinCode2.Text = trailer.bin_code;
                    tvItemNo2.Text = trailer.item_no;
                    tvItemDesc2.Text = trailer.item_desc;
                    tvBlocked2.Text = trailer.blocked;               
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }
    }
}