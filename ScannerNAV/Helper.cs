﻿using Android.App;
using Android.Content;
using ScannerNAV.Webservice;
using Android.Widget;
using Android.Graphics;
using Android.Views;
using System.Collections.Generic;

namespace ScannerNAV
{
    public class Helper
    {
        internal static void ShowAlertDialog(Context Context,string Title,string Message)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(Context);
            AlertDialog alert = dialog.Create();
            alert.SetTitle(Title);
            alert.SetMessage(Message);
            alert.SetButton("OK", (c, ev) => { });
            alert.Show();
        }

        internal static void ShowToast(Context Context, string status_text)
        {
            Toast toast = Toast.MakeText(Context, status_text, ToastLength.Long);
            toast.View.SetBackgroundColor(Color.Green);
            TextView view = (TextView)toast.View.FindViewById(Android.Resource.Id.Message);            
            view.SetTextColor(Color.Black);
            toast.Show();
        }

        internal static ScannerInterface GetInterface(Context Context)
        {
            ScannerInterface ws = new ScannerInterface
            {
                Credentials = new System.Net.NetworkCredential(GetWSUser(), GetWSPassword()),
                Url = GetWSUrl()
            };
            return ws;
        }

        internal static string GetStatus()
        {
            return GetRescourceNo();
        }

        internal static bool IsOK(string Value)
        {
            if (Value == "ok")
                return true;
            else
                return false;
        }

        internal static bool IsNeddInfo(string Value)
        {
            if (Value == "needInfo")
                return true;
            else
                return false;
        }

        // WSUser
        internal static string GetWSUser()
        {
            return GetKey("WSUser");
        }
        internal static void SetWSUser(string KeyValue)
        {
            SetKey("WSUser",KeyValue);
        }
        // WSPassword
        internal static string GetWSPassword()
        {
            return GetKey("WSPassword");
        }
        internal static void SetWSPasword(string KeyValue)
        {
            SetKey("WSPassword", KeyValue);
        }
        // WSUrl
        internal static string GetWSUrl()
        {
            return GetKey("WSUrl");
        }
        internal static void SetWSUrl(string KeyValue)
        {
            SetKey("WSUrl", KeyValue);
        }
        // RescourceNo
        internal static string GetRescourceNo()
        {
            return GetKey("RescourceNo");
        }
        internal static void SetRescourceNo(string KeyValue)
        {
            SetKey("RescourceNo", KeyValue);
        }

        internal static string GetVersion(Context Context)
        {
            return "Version: "
                + Context.PackageManager.GetPackageInfo(Context.PackageName, 0).VersionName
                + Context.PackageManager.GetPackageInfo(Context.PackageName, 0).VersionCode;
        }

        private static void SetKey(string KeyName, string KeyValue)
        {
            var prefs = Application.Context.GetSharedPreferences("ScannerNAV", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString(KeyName, KeyValue);
            prefEditor.Commit();
        }

        private static string GetKey(string KeyName)
        {
            //retreive 
            var prefs = Application.Context.GetSharedPreferences("ScannerNAV", FileCreationMode.Private);
            return prefs.GetString(KeyName, null);
        }
    }

    public class CommentModel
    {
        public string Date { get; set; }
        public string Comment { get; set; }
    }

    public class CommentModelAdapter : BaseAdapter<CommentModel>
    {
        private readonly IList<CommentModel> _items;
        private readonly Context _context;

        public CommentModelAdapter(Context context, IList<CommentModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.TrailerCommentList, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.CommentDate).Text = item.Date;
            view.FindViewById<TextView>(Resource.Id.CommentText).Text = item.Comment;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override CommentModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class ItemsModel
    {
        public string ItemNo { get; set; }
        public string ItemDesc { get; set; }
        public string ItemDesc2 { get; set; }
        public string Avalible_stock { get; set; }
        public string Qty_purch_order { get; set; }
        public string Unit_price { get; set; }
        public string Unit_price_vat { get; set; }
        public string Cost_code { get; set; }
    }

    public class ItemsModelAdapter : BaseAdapter<ItemsModel>
    {
        private readonly IList<ItemsModel> _items;
        private readonly Context _context;

        public ItemsModelAdapter(Context context, IList<ItemsModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.Items, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.ItemNo).Text = item.ItemNo;
            view.FindViewById<TextView>(Resource.Id.ItemDesc).Text = item.ItemDesc;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override ItemsModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class ItemLocationModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Qty { get; set; }
    }

    public class ItemLocationModelAdapter : BaseAdapter<ItemLocationModel>
    {
        private readonly IList<ItemLocationModel> _items;
        private readonly Context _context;

        public ItemLocationModelAdapter(Context context, IList<ItemLocationModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.Locations, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.Name).Text = item.Name;
            view.FindViewById<TextView>(Resource.Id.Qty).Text = item.Qty;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override ItemLocationModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class PurcahseOrderModel
    {
        public int LineNo { get; set; }
        public string ItemNo { get; set; }
        public string ItemDesc { get; set; }
        public string OutQty { get; set; }
    }

    public class PurcahseOrderModelAdapter : BaseAdapter<PurcahseOrderModel>
    {
        private readonly IList<PurcahseOrderModel> _items;
        private readonly Context _context;

        public PurcahseOrderModelAdapter(Context context, IList<PurcahseOrderModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.PurchaseOrderLines, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.ItemNo).Text = item.ItemNo;
            view.FindViewById<TextView>(Resource.Id.ItemDesc).Text = item.ItemDesc;
            view.FindViewById<TextView>(Resource.Id.OutQty).Text = item.OutQty;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override PurcahseOrderModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class ResourcesearchModel
    {
        public string ResourceNo { get; set; }
        public string ResourceName { get; set; }
    }

    public class ResourcesearchModelAdapter : BaseAdapter<ResourcesearchModel>
    {
        private readonly IList<ResourcesearchModel> _items;
        private readonly Context _context;

        public ResourcesearchModelAdapter(Context context, IList<ResourcesearchModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.Resources, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.ResourceNo).Text = item.ResourceNo;
            view.FindViewById<TextView>(Resource.Id.ResourceName).Text = item.ResourceName;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override ResourcesearchModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class TrailerInfoModel
    {
        public string TrailerID { get; set; }
        public string Serial { get; set; }
    }

    public class TrailerInfoModelAdapter : BaseAdapter<TrailerInfoModel>
    {
        private readonly IList<TrailerInfoModel> _items;
        private readonly Context _context;

        public TrailerInfoModelAdapter(Context context, IList<TrailerInfoModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.TrailerInfoList, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.TrailerID).Text = item.TrailerID;
            view.FindViewById<TextView>(Resource.Id.Serial).Text = item.Serial;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override TrailerInfoModel this[int position]
        {
            get { return _items[position]; }
        }
    }   

    public class SalesOrderModel
    {
        public string OrderNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
    }

    public class SalesOrderModelAdapter : BaseAdapter<SalesOrderModel>
    {
        private readonly IList<SalesOrderModel> _items;
        private readonly Context _context;

        public SalesOrderModelAdapter(Context context, IList<SalesOrderModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.Orders, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.No).Text = item.OrderNo;
            view.FindViewById<TextView>(Resource.Id.CustomerInfo).Text = item.CustomerNo + " " + item.CustomerName;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override SalesOrderModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class SalesOrderLineModel
    {
        public int LineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string quantity { get; set; }
        public string location_code { get; set; }
        public string LineType { get; set; }
    }

    public class SalesOrderLineModelAdapter : BaseAdapter<SalesOrderLineModel>
    {
        private readonly IList<SalesOrderLineModel> _items;
        private readonly Context _context;

        public SalesOrderLineModelAdapter(Context context, IList<SalesOrderLineModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.OrderLines, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.ItemNo).Text = item.ItemNo;
            view.FindViewById<TextView>(Resource.Id.ItemDesc).Text = item.Description;
            view.FindViewById<TextView>(Resource.Id.QtyInfo).Text = "Antal: " + item.quantity + "   Lokation: " + item.location_code;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override SalesOrderLineModel this[int position]
        {
            get { return _items[position]; }
        }
    }

    public class LocationModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class LocationModelAdapter : BaseAdapter<LocationModel>
    {
        private readonly IList<LocationModel> _items;
        private readonly Context _context;

        public LocationModelAdapter(Context context, IList<LocationModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.Locations, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.Name).Text = item.Name;
            view.FindViewById<TextView>(Resource.Id.Qty).Visibility = ViewStates.Gone;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override LocationModel this[int position]
        {
            get { return _items[position]; }
        }
    }
}