﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Søg vare")]
    public class Locations : Activity
    {
        private List<LocationModel> itemList;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.ItemLocation);            

            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                location_root ResponseRoot = new location_root();

                ws.GetLocation(ref ResponseRoot);

                location_response XmlResponse = ResponseRoot.location_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    itemList = new List<LocationModel>(XmlResponse.location.Length);
                    foreach (location item in XmlResponse.location)
                    {
                        itemList.Add(new LocationModel
                        {
                            Code = item.no,
                            Name = item.name
                        });
                    }
                    FindViewById<ListView>(Resource.Id.listView).Adapter = new LocationModelAdapter(this, itemList);
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }

            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }

            FindViewById<ListView>(Resource.Id.listView).ItemClick += ListView_ItemClick;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            LocationModel selectedItem = new LocationModel();
            selectedItem = itemList[e.Position];
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            createPurchaseLine.PutExtra("LocationNo", selectedItem.Code);
            SetResult(Result.Ok, createPurchaseLine);
            Finish();
        }

        override public void OnBackPressed()
        {
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            SetResult(Result.Canceled, createPurchaseLine);
            Finish();
        }
    }
}