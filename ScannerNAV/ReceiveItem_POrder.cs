﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "ReceiveItem")]
    public class ReceiveItem_POrder : Activity
    {
        private Button btnNewLine;
        private TextView tvPONo;
        private ListView listView;
        List<PurcahseOrderModel> items;
        private purchase_Lines[] purchase_Lines;
        private string purchaseNo;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);           

            SetContentView(Resource.Layout.PurchaseOrder);
            

            purchaseNo = Intent.GetStringExtra("PONo") ?? "Data not available";

            tvPONo = FindViewById<TextView>(Resource.Id.tvPONo);
            tvPONo.Text = purchaseNo;

            listView = FindViewById<ListView>(Resource.Id.listView);
            listView.ItemClick += ListView_ItemClick;

            btnNewLine = FindViewById<Button>(Resource.Id.btnNewLine);
            btnNewLine.Click += BtnNewLine_Click; ;
            
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                purchase_Lines_root purchaseLinesResponseRoot = new purchase_Lines_root();

                ws.GetPurchaseLines(ref purchaseLinesResponseRoot, purchaseNo);
                
                try
                {
                    purchase_Lines_response purchaseOrderXmlResponse = purchaseLinesResponseRoot.purchase_Lines_response[0];

                    if (Helper.IsOK(purchaseOrderXmlResponse.status))
                    {                        
                        string ResourceNo = Helper.GetRescourceNo();
                        items = new List<PurcahseOrderModel>(purchaseOrderXmlResponse.purchase_Lines.Length);
                        purchase_Lines = purchaseOrderXmlResponse.purchase_Lines;                        
                        foreach (purchase_Lines item in purchaseOrderXmlResponse.purchase_Lines)
                        {
                            items.Add(new PurcahseOrderModel { LineNo = item.LineNo, ItemNo = item.ItemNo, ItemDesc = item.ItemDescription, OutQty = item.OutstandingQty });
                        }
                        listView.Adapter = new PurcahseOrderModelAdapter(this, items);
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, purchaseOrderXmlResponse.status, purchaseOrderXmlResponse.status_text);
                    }
                }
                catch (Exception)
                {
                    Helper.ShowToast(this, "Ingen linjer blev fundet");
                    Finish();
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void BtnNewLine_Click(object sender, EventArgs e)
        {
            Intent createPurchaseLine = new Intent(this, typeof(PurchaseLineCard));
            createPurchaseLine.PutExtra("PONo", purchaseNo);
            StartActivity(createPurchaseLine);
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            PurcahseOrderModel selectedItem = new PurcahseOrderModel();
            selectedItem = items[e.Position];
            Intent receiveItem_POrder = new Intent(this, typeof(ReceiveItemCard));
            receiveItem_POrder.PutExtra("PONo", purchaseNo);
            receiveItem_POrder.PutExtra("LineNo", selectedItem.LineNo.ToString());
            StartActivityForResult(receiveItem_POrder, 1);            
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if ((requestCode == 1) & (resultCode == Result.Ok))
            {
                Recreate();
            }
        }
    }
}