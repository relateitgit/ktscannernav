﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;

namespace ScannerNAV
{
    [Activity(Label = "Modtage vare", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ReceiveItemCard : Activity
    {
        private Button btnReceive;
        private EditText etSerial;
        private Spinner spnSerial;
        private TextView tvStatus;
        private reservation_entry[] trackingSpecification;
        private string purchaseNo;
        private int LineNo;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            purchaseNo = Intent.GetStringExtra("PONo") ?? "Data not available";
            Int32.TryParse(Intent.GetStringExtra("LineNo") ?? "Data not available", out LineNo);

            SetContentView(Resource.Layout.ReveiveItemCard);

            spnSerial = FindViewById<Spinner>(Resource.Id.spnSerial);
            etSerial = FindViewById<EditText>(Resource.Id.etSerial);
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                reservation_entry_root navResponse = new reservation_entry_root();

                ws.GetSerial(ref navResponse, purchaseNo, LineNo);

                reservation_entry_response xmlResponse = navResponse.reservation_entry_response[0];
                
                if (Helper.IsOK(xmlResponse.status))
                {
                    int counter = 0;
                    string CurrBin = xmlResponse.status_text;
                    List<string> items = new List<string>(xmlResponse.reservation_entry.Length);
                    trackingSpecification = xmlResponse.reservation_entry;
                    foreach (reservation_entry item in xmlResponse.reservation_entry)
                    {
                        items.Add(item.serialNo);
                        counter++;
                    }
                    ArrayAdapter adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                    spnSerial.Adapter = adapter;

                    spnSerial.Visibility = ViewStates.Visible;
                    spnSerial.RequestFocus();

                    etSerial.Visibility = ViewStates.Gone;
                }
                else if (Helper.IsNeddInfo(xmlResponse.status))
                {
                    spnSerial.Visibility = ViewStates.Gone;
                    etSerial.Visibility = ViewStates.Visible;
                    etSerial.KeyPress += OnSerialText_KeyPress;
                    etSerial.RequestFocus();                    
                }
                else
                {
                    Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                    SetResult(Result.Canceled);
                    Finish();
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }

            btnReceive = FindViewById<Button>(Resource.Id.btnReceive);
            btnReceive.Click += OnReceiveClick;

            tvStatus = FindViewById<TextView>(Resource.Id.tvStatus);
            tvStatus.Click += OnStatusClick;
            tvStatus.Text = Helper.GetStatus();
        }

        private void OnSerialText_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                btnReceive.CallOnClick();                
            }
            else
                e.Handled = false;
        }

        private void OnReceiveClick(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                if (etSerial.Text == "")
                {
                    ws.ReceiveItemWithSerial(ref navResponse, purchaseNo, LineNo, trackingSpecification[spnSerial.SelectedItemPosition].serialNo);
                }
                else
                {
                    ws.ReceiveItemWithSerial(ref navResponse, purchaseNo, LineNo, etSerial.Text);                    
                }

                default_response xmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(xmlResponse.status))
                {
                    Helper.ShowToast(this, xmlResponse.status_text);
                    SetResult(Result.Ok);
                    Finish();
                }
                else
                {
                    Helper.ShowAlertDialog(this, xmlResponse.status, xmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            tvStatus.Text = Helper.GetStatus();
        }
    }
}