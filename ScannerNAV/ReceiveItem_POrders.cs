﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "ReceiveItem")]
    public class ReceiveItem_POrders : Activity
    {
        private EditText etSearchItemNo;
        private ListView listView;
        List<PurcahseOrdersModel> items;
        private purchase_Orders[] purchase_Orders;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.ReveiveItemList);

            etSearchItemNo = FindViewById<EditText>(Resource.Id.etSearchItemNo);
            etSearchItemNo.KeyPress += OnSearchItemNo_KeyPress;
            etSearchItemNo.RequestFocus();

            listView = FindViewById<ListView>(Resource.Id.listView);
            listView.ItemClick += ListView_ItemClick;
        }

        private void OnSearchItemNo_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;

                try
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    purchase_Orders_root purchaseOrdersResponseRoot = new purchase_Orders_root();

                    ws.GetPurchaseOrders(ref purchaseOrdersResponseRoot,etSearchItemNo.Text);

                    try
                    {
                        purchase_Orders_response purchaseOrderXmlResponse = purchaseOrdersResponseRoot.purchase_Orders_response[0];

                        if (Helper.IsOK(purchaseOrderXmlResponse.status))
                        {
                            string ResourceNo = Helper.GetRescourceNo();
                            items = new List<PurcahseOrdersModel>(purchaseOrderXmlResponse.purchase_Orders.Length);
                            purchase_Orders = purchaseOrderXmlResponse.purchase_Orders;
                            foreach (purchase_Orders item in purchaseOrderXmlResponse.purchase_Orders)
                            {
                                items.Add(new PurcahseOrdersModel { PONo = item.no, POVendorName = item.vendor_name });
                            }
                            listView.Adapter = new PurcahseOrdersModelAdapter(this, items);
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, purchaseOrderXmlResponse.status, purchaseOrderXmlResponse.status_text);
                        }
                    }
                    catch (Exception)
                    {
                        Helper.ShowAlertDialog(this, "Information", "Ingen ordre blev fundet");
                    }


                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
            else
                e.Handled = false;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            PurcahseOrdersModel selectedItem = new PurcahseOrdersModel();
            selectedItem = items[e.Position];
            Intent receiveItem_POrder = new Intent(this, typeof(ReceiveItem_POrder));
            receiveItem_POrder.PutExtra("PONo", selectedItem.PONo);
            StartActivity(receiveItem_POrder);            
        }
    }

    public class PurcahseOrdersModel
    {
        public string PONo { get; set; }
        public string POVendorName { get; set; }
    }

    public class PurcahseOrdersModelAdapter : BaseAdapter<PurcahseOrdersModel>
    {
        private readonly IList<PurcahseOrdersModel> _items;
        private readonly Context _context;

        public PurcahseOrdersModelAdapter(Context context, IList<PurcahseOrdersModel> items)
        {
            _items = items;
            _context = context;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                var inflater = LayoutInflater.FromContext(_context);
                view = inflater.Inflate(Resource.Layout.PurchaseOrders, parent, false);
            }

            view.FindViewById<TextView>(Resource.Id.PONo).Text = item.PONo;
            view.FindViewById<TextView>(Resource.Id.POVendorName).Text = item.POVendorName;

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override PurcahseOrdersModel this[int position]
        {
            get { return _items[position]; }
        }
    }
}